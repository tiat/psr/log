<?php

/**
 * PHP FIG
 *
 * @category     PSR-3
 * @package      Log
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\Log;

/**
 * Describes log levels.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class LogLevel {
	
	/**
	 * Urgent alert.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EMERGENCY = 'emergency';
	
	/**
	 * Action must be taken immediately
	 * Example: Entire website down, database unavailable, etc.
	 * This should trigger the SMS alerts and wake you up.
	 * This should trigger the SMS alerts and wake you up.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string ALERT = 'alert';
	
	/**
	 * Critical conditions
	 * Example: Application component unavailable, unexpected exception.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string CRITICAL = 'critical';
	
	/**
	 * Runtime errors
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string ERROR = 'error';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string WARNING = 'warning';
	
	/**
	 * Uncommon events
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string NOTICE = 'notice';
	
	/**
	 * Interesting events
	 * Example the logs
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INFO = 'info';
	
	/**
	 * Detailed debug information
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEBUG = 'debug';
}
