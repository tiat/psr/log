<?php

/**
 * PHP FIG
 *
 * @category     PSR-3
 * @package      Log
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\Log;

//
use Psr\Log\LoggerInterface;

/**
 * Describes a logger-aware instance.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface LoggerAwareInterface extends \Psr\Log\LoggerAwareInterface {
	
	/**
	 * Sets a logger instance on the object.
	 *
	 * @param    LoggerInterface    $logger
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setLogger(LoggerInterface $logger) : void;
}
